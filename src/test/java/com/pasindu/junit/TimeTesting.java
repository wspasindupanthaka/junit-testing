package com.pasindu.junit;

import org.junit.jupiter.api.*;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public class TimeTesting {

    @Test
    @Timeout(value = 1, unit = TimeUnit.MILLISECONDS)
    public void testSoft_Performance() {
        int array[] = {4, 8, 10};
        for (int i = 0; i < 1000000; i++) {
            array[0] = i;
            Arrays.sort(array);
        }
    }

}
