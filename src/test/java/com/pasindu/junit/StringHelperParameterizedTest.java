package com.pasindu.junit;


import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class StringHelperParameterizedTest {

   static StringHelper helper;

    @BeforeAll
    public static void setup() {
        helper = new StringHelper();
    }

    @ParameterizedTest
    @CsvSource({"AACD, CD", "ACD, CD"})
    public void testTruncateAInFirst2Positions(String input, String expectedOutput) {
        assertEquals(expectedOutput, helper.truncateAInFirst2Positions(input));
    }


}
