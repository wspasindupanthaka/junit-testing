package com.pasindu.junit;

import org.junit.jupiter.api.*;

public class BeforeAfterTest {

    @BeforeAll
    public static void beforeClass() {
        System.out.println("beforeClass");
    }

    @BeforeEach
    public void before() {
        System.out.println("before");
    }

    @Test
    public void test1() {
        System.out.println("test1");
    }

    @Test
    public void test2() {
        System.out.println("test2");
    }

    @AfterEach
    public void after() {
        System.out.println("after");
    }

    @AfterAll
    public static void afterClass() {
        System.out.println("afterClass");
    }

}
