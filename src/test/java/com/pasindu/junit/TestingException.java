package com.pasindu.junit;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestingException {

    StringHelper helper= new StringHelper();

    @Test
    public void testTruncateAInFirst2Positions_NullInput() {
        assertThrows(NullPointerException.class,
                () -> {
                    helper.truncateAInFirst2Positions(null);
                });
    }

    @Test
    public void testTruncateAInFirst2Positions_AInBothFirst2Positions() {
        assertThrows(NullPointerException.class,
                () -> {
                    helper.truncateAInFirst2Positions("AACD");
                });
    }

}
