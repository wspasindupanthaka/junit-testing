package com.pasindu.junit;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class StringHelperTest {

    StringHelper helper;

    @BeforeEach
    public void setup() {
        helper = new StringHelper();
    }

    @Test
    public void test() {

    }

    @Test
    public void testTruncateAInFirst2Positions_AInBothFirst2Positions() {

        assertEquals("CD", helper.truncateAInFirst2Positions("AACD"));
    }

    @Test
    public void testTruncateAInFirst2Positions_AInOneOfTheFirst2Positions() {
        assertEquals("CD", helper.truncateAInFirst2Positions("ACD"));
    }

    //ABCD => false, ABAB => false, AB => true, A => false
    @Test
    public void testAreFirstAndLastTwoCharactersTheSame_GeneralNegativeCondition() {
        assertFalse(helper.areFirstAndLastTwoCharactersTheSame("ABCD"));
    }

    @Test
    public void testAreFirstAndLastTwoCharactersTheSame_GeneralPositiveCondition() {
        assertTrue(helper.areFirstAndLastTwoCharactersTheSame("ABAB"));
    }

    @Test
    public void testTruncateAInFirst2Positions_NullInput() {

        assertEquals("CD", helper.truncateAInFirst2Positions(null));
    }

}
